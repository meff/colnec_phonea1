'use strict';

function loginService(httpService, $q, $localStorage) {

    this.Get_API_Session = function(user) {

        var deferred = $q.defer();

        var promise = httpService.Call('POST', 'session?username=' + user.username + '&password=' + user.password);

        promise.then(function(result) {
            deferred.resolve(result);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }


    this.Get_Local_Session = function() {
        return $localStorage.$default({
            sessionId: ''
        });
    }

    //loginService.Del_Local_Session();
    this.Del_Local_Session = function() {

        $localStorage.$reset({
            sessionId: ''
        });
        console.log("Del_Local_Session", $localStorage);
    }
}

colnecMobile.service('loginService', loginService);
