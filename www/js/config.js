'use strict';

var ROOT = "html/";

function config($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom');

    //$ionicConfigProvider.views.maxCache(0);

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: ROOT + "login.html",
            controller: 'loginCtrl'
            // cache: false
        })
        .state('tabs', {
            url: "/tab",
            abstract: true,
            templateUrl: ROOT + "tabs.html",
            controller: 'tabsCtrl'
        })
        .state('tabs.actions', {
            url: "/actions",
            views: {
                'actions-tab': {
                    templateUrl: ROOT + "actions.html",
                    controller:"actionsCtrl"
                }
            }
        })
        .state('tabs.actionsQuestionToAsk', {
            url: "/actionsQuestionToAsk",
            views: {
                'actions-tab': {
                    templateUrl: ROOT + "actionsQuestionToAsk.html",
                    controller:"actionsQuestionToAskCtrl"
                }
            }
        })
        .state('tabs.actionsMesurerBloodPressure', {
            url: "/actionsMesurerBloodPressure",
            views: {
                'actions-tab': {
                    templateUrl: ROOT + "actionsMesurerBloodPressure.html",
                    controller:"actionsMesurerBloodPressureCtrl"
                }
            }
        })
        .state('tabs.journal', {
            url: "/journal",
            views: {
                'journal-tab': {
                    templateUrl: ROOT + "journal.html",
                    controller:"journalCtrl"
                }
            }
        })
        .state('tabs.journalAddEdit', {
            url: "/journalAddEdit",
            views: {
                'journal-tab': {
                    templateUrl: ROOT + "journalAddEdit.html",
                    controller:"journalAddEditCtrl"
                }
            }
        })
        .state('tabs.messages', {
            url: "/messages",
            views: {
                'messages-tab': {
                    templateUrl: ROOT + "messages.html",
                    controller:"messagesCtrl"
                }
            }
        })
        .state('tabs.messagesAddEdit', {
            url: "/messagesAddEdit",
            views: {
                'messages-tab': {
                    templateUrl: ROOT + "messagesAddEdit.html",
                    controller:"messagesAddEditCtrl"
                }
            }
        })
        .state('tabs.dashboard', {
            url: "/dashboard",
            views: {
                'dashboard-tab': {
                    templateUrl: ROOT + "dashboard.html",
                    controller:"dashboardCtrl"
                }
            }
        });


}


colnecMobile
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
