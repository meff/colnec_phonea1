'use strict';

function httpService($q, $sessionStorage, $http, apiUrl) {

    this.Call = function (method, path) {

        $http.defaults.useXDomain = true;
        delete $http.defaults.headers.common['X-Requested-With'];

        $http.defaults.headers.common = {'Ehr-Session':$sessionStorage.$default({sessionId : ''}).sessionId};

        var deferred = $q.defer();


        //TODO : Can't use http call without CORS chrome extension
        $http({
            method : method,
            url : apiUrl + path
        }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

}


colnecMobile
    .service('httpService', httpService);
