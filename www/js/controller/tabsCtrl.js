'use strict';

function tabsCtrl($scope, $state, $window, loginService) {
  $scope.$storage = {};
    $scope.onLogoutClicked = function() {

        loginService.Del_Local_Session();
        $state.go('login',{},{ reload: true, inherit: true, notify: true });
    }
}

colnecMobile.controller('tabsCtrl', tabsCtrl);
