'use strict';

function journalCtrl($scope, $state, $window, $timeout) {
    $scope.STATUS_DELETE = -1;
    $scope.STATUS_EDIT = 1;
    $scope.STATUS_NONE = 0;

    $scope.itemStatus = $scope.STATUS_NONE;
    $scope.items = [];

    for (var i = 0; i < 10; i++) {
        var rowItem = {};
        rowItem.date = "21.12.15";
        rowItem.time = "12:33";
        rowItem.discomfortLevel = 3;
        rowItem.descriptSymptom = "No." + i + " ##### Difficulté avec marche à pied";
        rowItem.descriptHappened = "J'ai pris un american breakfast sans sel au petit déjeuner";

        $scope.items.push(rowItem);
    }



    $scope.onRowClicked = function(item, at) {

        //$window.location.href = '../../html/signIn.html';
    }

    $scope.onRowDeleteClicked = function(item, at) {
        $timeout(function() {
            $scope.items.splice($scope.items.indexOf(item), 1);
        }, 300);
    }

    $scope.onRowEditClicked = function(item, at) {
        $window.location.href = '#/tab/journalAddEdit';
    }

    $scope.onFooterDeleteClicked = function() {
        $scope.itemStatus = $scope.itemStatus == $scope.STATUS_DELETE ? $scope.STATUS_NONE : $scope.STATUS_DELETE;
    }

    $scope.onFooterEditClicked = function() {
        $scope.itemStatus = $scope.itemStatus == $scope.STATUS_EDIT ? $scope.STATUS_NONE : $scope.STATUS_EDIT;
    }
}


colnecMobile.controller('journalCtrl', journalCtrl);
