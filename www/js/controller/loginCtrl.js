'use strict';

function loginCtrl($scope, $state, $sessionStorage, $window, loginService, eMessage) {


    $scope.$storage = {};
    $scope.user = {};


    $scope.$on('$ionicView.enter', function() {

        $scope.$storage = loginService.Get_Local_Session();

        console.log("start login",$scope.$storage);
        
        $scope.user = {
            username: $scope.$storage.user.username,
            password: $scope.$storage.user.password,
            isRememberUser: $scope.$storage.user.isRememberUser
        };

        if ($scope.$storage.sessionId) {
            $state.go('tabs.actions');
        }

    });

    $scope.submit = function() {

        if ($scope.user.isRememberUser) {
          console.log("true",$scope.user.isRememberUser);
            $scope.$storage.user = $scope.user;
        } else {
          console.log("false",$scope.user.isRememberUser);
            $scope.$storage.user = {};
        }

        var promise = loginService.Get_API_Session($scope.user);

        promise.then(function(result) {

            $scope.$storage.sessionId = result.data.sessionId;

            $state.go('tabs.actions');

        }, function(error) {
            alert(error[eMessage]);
        });
    }
}

colnecMobile.controller('loginCtrl', loginCtrl);
